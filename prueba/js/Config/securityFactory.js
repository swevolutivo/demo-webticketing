var requestServices = angular.module('securityModule', [ 'ngResource' ]);


requestServices
		.factory(
				'securityFactory',
				[
						'$resource', '$http',
						function($resource, $http) {
							//$http.defaults.withCredentials = true;
							var result = {"fullName":"Aguilar-Bravo, Jorge Hernán","groups":[{"name":"WebLogicADusers"},{"name":"EC Cons"},{"name":"Courier Administrator"},{"name":"Courier Assistant"},{"name":"Courier Messenger"},{"name":"Ecu_InternetAccess_Technical_Users"},{"name":"EC All"},{"name":"Courier Reception"},{"name":"EC Quito Cons"},{"name":"CTX-Access-Remote_Advanced"},{"name":"Web Ticketing on Behalf"},{"name":"CTX-Web Mail"},{"name":"ECU_Andes_Users"},{"name":"TRF Administrators"},{"name":"EC Quito All"}],"userName":"jaguila1"};
							return result;
						} ]);
