angular.module('formApp')

// our controller for the form
// =============================================================================
.controller('loginController',['$scope','$http','securityFactory',function($scope,$http,securityFactory){
	
	//console.log(securityFactory); 
	$scope.userCaller = securityFactory.userName;
	$scope.userFullName = securityFactory.fullName;
	

	
	
	
	//Change the language of the static words
    $scope.changeLanguage = function (key) {
        $translate.use(key);
    };    
    
    $scope.languageId = "eng";
    $scope.textModel;
    $scope.reqTypId = 281485929021545;    
    $scope.idioma = "Español";
    $scope.codigoIdioma = "en"
    
    $scope.setLanguage = function(){
    	if($scope.idioma == "Español"){
    		$scope.idioma = "English";
    		$scope.languageId = "spa";
    		$scope.codigoIdioma = "sp";
    		$scope.nombreCategoria = "";
    		$scope.nombreSubcategoria = "";
            $scope.nombreAccion = "";
            $scope.description = "";
            $scope.actions = "";
            $scope.subcategories = "";
    		return $scope.varIdioma = {'languageId':$scope.languageId, 'codigoIdioma':$scope.codigoIdioma};
    		   		
    	}else{
    		$scope.idioma = "Español";    		
    		$scope.languageId = "eng";
    		$scope.codigoIdioma = "en";
    		$scope.nombreCategoria = "";
    		$scope.nombreSubcategoria = "";
            $scope.nombreAccion = "";
            $scope.description = "";
            $scope.actions = "";
            $scope.subcategories = "";           
    		return $scope.varIdioma = {'languageId':$scope.languageId, 'codigoIdioma':$scope.codigoIdioma};
    	}
    };	
    
    
    $scope.getLanguage = function (value) {
        $scope.languageId = value;
        getCategories();
    };  
    
    
    
    
    
    var getCategories = function(){

    	//alert('Cargando categories');
    	
    	$http.get("restapi/categories/byreqtype?reqtype=281485929021545&language=" + $scope.languageId).success(function (data) {
            console.log(data);
            $scope.categories = data;           
        }).error(function (err) {
        	console.log(err);
        });
    }
    
    
    getCategories();
    
    
    $scope.selectedCategory = {id:99};
    
	
	
		
}])