// create our angular app and inject ngAnimate and ui-router
// =============================================================================
var app = angular.module('formApp')

// our controller for the form
// =============================================================================
app.controller('formController',['$scope','$http','$translate','$filter', 'securityFactory', function($scope, $http, $translate, $filter, securityFactory) {



	//console.log(securityFactory);
	$scope.userCaller = securityFactory.userName;
	$scope.userFullName = securityFactory.fullName;
	$scope.groups = securityFactory.groups;
	// $scope.userCaller = 'JAGUILA1';
	// $scope.userFullName = 'Usuario quemado';

	var inGroup = function(JSONArray, groupToFind){
	    for (var i = 0; i < JSONArray.length; i++) {
	    	//console.log(JSONArray[i].name);
	    	if (JSONArray[i].name == groupToFind)
	    		return true;
	    	}
	    return false;
	};

	$scope.isAnalyst = inGroup($scope.groups, 'Web Ticketing on Behalf');


	//Change the language of the static words
    $scope.changeLanguage = function (key) {
        $translate.use(key);
    };

    $scope.languageId = "eng";
    $scope.reqTypId = 281485929021545;
    $scope.idioma = "Español";
    $scope.codigoIdioma = "en"

    $scope.setLanguage = function(){
    	if($scope.idioma == "Español"){
    		$scope.idioma = "English";
    		$scope.languageId = "spa";
    		$scope.codigoIdioma = "sp";
    		return $scope.varIdioma = {'languageId':$scope.languageId, 'codigoIdioma':$scope.codigoIdioma};

    	}else{
    		$scope.idioma = "Español";
    		$scope.languageId = "eng";
    		$scope.codigoIdioma = "en";
    		return $scope.varIdioma = {'languageId':$scope.languageId, 'codigoIdioma':$scope.codigoIdioma};
    	}
    };


    $scope.getLanguage = function (value) {
        $scope.languageId = value;
        $scope.formData.selectedCategory = {};
        $scope.formData.selectedSubcategory = {};
    	$scope.formData.selectedAction = {};
        getCategories();
    };


 // Upload file form
    $scope.formData = {};


    var getCategories = function(){

    	$http.get("restapi/categories/byreqtype?reqtype=281485929021545&language=" + $scope.languageId).success(function (data) {
            console.log(data);
            $scope.categories = data;
        }).error(function (err) {
        	console.log(err);
        });

    }


    getCategories();


    $scope.getSubcategories = function (categoryId) {

    	// Clear subcategories and actions
    	$scope.formData.selectedSubcategory = {};
    	$scope.formData.selectedAction = {};
    	$scope.formData.textModel = "";
    	$scope.actions = {};

      $http.get("restapi/categories/subcategories/bycat?category=" + categoryId + "&language=" + $scope.languageId).success(
      function (response) {
          //console.log(response);
          $scope.subcategories = response;

      }).error(function (error, status, headers, config) {
          console.log(error);
      });
    };


    $scope.getActions = function (subcategoryId) {

    	// Clear actions
    	$scope.formData.selectedAction = {};

        $http.get("restapi/categories/actions/bysubcat?subcategory=" + subcategoryId + "&language=" + $scope.languageId).success(
        function (response) {
        	console.log(response);
            $scope.actions = response;


            // Default action
            $scope.formData.selectedAction = $filter('filter')($scope.actions.actionAssignmentDTO, {defaultSelected: 'Y'})[0];

            if($scope.formData.selectedAction)
            	$scope.setTemplate();


            console.log("Acciones");
            console.log($scope.selectedAction);

            console.log("template");


        }).error(function (error, status, headers, config) {
            console.log(error);
        });
    };


    $scope.setTemplate = function() {

    	if($scope.formData.selectedAction.templateContent){
        	$scope.formData.textModel = $scope.formData.selectedAction.templateContent;
        } else{
        	$scope.formData.textModel = "";
        }
    };



    $scope.uploadFile = function(){
    	//$("#enviarForm").appendTo("#myForm");
    	//var data = $("#myForm").serialize();
    	var data = new FormData();

    	var fileName = $("#enviarForm").get(0).files[0].name;
    	data.append('file', $("#enviarForm").get(0).files[0], fileName);
    	$.ajax({
    		type: 'POST',
    		url: 'restapi/file/upload',
    		data: data,
    		processData: false,
    		contentType: false,
    		success: function (data) {
				$scope.retornoNombre = data.fileName;
				$scope.retornoNombreOriginal = data.fileNameOriginal;
			},
    		error: function(data){
    			alert('Failed');
    		},
    		dataType: 'json',
    	});
    }


    $scope.validate = function () {
        console.log(/[{{}}]/.test($scope.formData.textModel));
        return /[{{}}]/.test($scope.formData.textModel);
//        return /[A-Za-z]\d{4}[\s|\-]*\d{5}[\s|\-]*\d{5}$/.test(value);

    };



    $scope.deleteScreen = function(){
    	$scope.retornoNombre = "";
    	$scope.retornoNombreOriginal = "";
    };


    $scope.getAlternativeCaller = function(selected){
    	console.log("CALLER: ");
    	console.log(selected);
    	if(selected !== undefined){
    		$scope.alternativeCallerUsername = selected.originalObject.username;
        	$scope.alternativeCallerName = selected.title;
    	}
    };

    $scope.clearRecipient = function(str) {
    	console.log("CLEAR:");
    	console.log(str);
    	$scope.alternativeCallerUsername = "";
    	$scope.alternativeCallerName = "";
    };

    $scope.sendTicket = function () {
    	var ticket = new Object();
        ticket.description = $scope.formData.textModel;

        ticket.initiator = $scope.userCaller;
        if($scope.alternativeCallerUsername) {
        	ticket.caller = $scope.alternativeCallerUsername;
        	ticket.initiator = $scope.userCaller;
        } else{
        	ticket.caller = $scope.userCaller;
        }

        ticket.language = $scope.languageId;
        ticket.category = $scope.formData.selectedCategory.id;
        ticket.subcategory = $scope.formData.selectedSubcategory.id;
        ticket.action = $scope.formData.selectedAction.id;
        if($scope.retornoNombre != ""){
        	ticket.fileName = $scope.retornoNombre;
        	ticket.fileNameOriginal = $scope.retornoNombreOriginal;
        }

        ticket.reqTyp = $scope.reqTypId;
        console.log(ticket);
        $http.post('restapi/tickets', ticket).success(function (data, status, headers, config) {
            if ($scope.idioma == "Español") {
            	alert("The ticket has been created");
			}else{
				alert("El ticket has sido creado!");
			}

            document.location.href = '#/form/category';
            location.reload();
        }).error(function (data, status, headers, config) {
            alert("failure message: " + JSON.stringify( {
                data : data
            }));
        });

    };


    $scope.orderName = function(element) {

		if (element.name.substring(0, 1) == ' ') {
			return 'ZZZZZZZZZZZZZZZZZZZZ';
		} else {
			return element.name;
		}
	}




}]);
