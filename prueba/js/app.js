// create our angular app and inject ngAnimate and ui-router 
// =============================================================================
angular.module('formApp', ['ngAnimate', 'ui.router', 'pascalprecht.translate'])

// configuring our routes 
// =============================================================================
.config(function($stateProvider, $urlRouterProvider, $translateProvider, $httpProvider) {
    
    $stateProvider
    
        // route to show our basic form (/form)
        .state('form', {
            url: '/form',
            templateUrl: 'form.html',
            //controller: 'formController'
        })
        
        // nested states 
        // each of these sections will have their own view
        // url will be nested (/form/profile)
        .state('form.category', {
        	
            url: '/category',
            templateUrl: 'form-category.html'
        })
        
        // url will be /form/interests
        .state('form.detail', {
            url: '/detail',
            templateUrl: 'form-detail.html'
        })
        
        // url will be /form/payment
        .state('form.summary', {
            url: '/summary',
            templateUrl: 'form-summary.html'
        });
       
    // catch all route
    // send users to the form page 
    $urlRouterProvider.otherwise('/form/category');
      
    
})

// our controller for the form
// =============================================================================
.controller('formController',function($scope, $http, $translate) {

	//Change the language of the static words
    $scope.changeLanguage = function (key) {
        $translate.use(key);
    };    
    
	
    $scope.categories = [];    
    $scope.subCategories = [];
    $scope.acciones = [];
    $scope.categoriesBackupEN = [];
    $scope.categoriesBackupSP = [];
        
    
    
    
    
    $scope.languageId = "eng";
    $scope.textModel;
    $scope.reqTypId = 281485929021545;    
    $scope.idioma = "Español";    
    
    $scope.setLanguage = function(){
    	if($scope.idioma == "Español"){
    		$scope.idioma = "English";
    		$scope.languageId = "spa";
    		$scope.codigoIdioma = "sp";
    		$scope.nombreCategoria = "";
    		$scope.nombreSubcategoria = "";
            $scope.nombreAccion = "";
            $scope.description = "";
    		return $scope.varIdioma = {'languageId':$scope.languageId, 'codigoIdioma':$scope.codigoIdioma};
    		   		
    	}else{
    		$scope.idioma = "Español";    		
    		$scope.languageId = 'eng';
    		$scope.codigoIdioma = "en";
    		$scope.nombreCategoria = "";
    		$scope.nombreSubcategoria = "";
            $scope.nombreAccion = "";
            $scope.description = "";
    		return $scope.varIdioma = {'languageId':$scope.languageId, 'codigoIdioma':$scope.codigoIdioma};
    	}
    };
    
    // we will store all of our form data in this object
    $scope.formData = {};    
    

    $scope.getLanguage = function (value) {
        $scope.languageId = value;
        console.log($scope.languageId);
        if (value == 1033) {
            $scope.categories = $scope.categoriesBackupEN;
            $scope.subCategories = "";
        }
        else {
            if ($scope.categoriesBackupSP != "") {
                $scope.categories = $scope.categoriesBackupSP;
                $scope.subCategories = "";
            }
            else {                
                $http.get("restapi/categories/query?reqTypId=281485929021545&languageId=" + $scope.languageId).success(function (data) {
                    console.log(data);
                    $scope.categories = data;
                    $scope.categoriesBackupSP = data;
                    $scope.subCategories = "";
                }).error(function (err) {

                });
            }

        }

    };    
   
    console.log($scope.languageId);
    $http.get("restapi/categories/query?reqTypOid=281485929021545&languageCode=" + $scope.languageId).success(function (data) {
        console.log("data"+data);
        
        $scope.categories = data;
        $scope.categoriesBackupEN = data;   
        
    }).error(function (err) {
    	alert("error");
    });

    $scope.getSubAct = function (valor) {       
        $http.get("restapi/categories/subcategoriesandactions/query?catId=" + valor + "&languageId=" + $scope.languageId).success(       
        function (data1) {
            console.log($scope.languageId);
            console.log(valor);
            $scope.subCategories = data1;
        }).error(function (error, status, headers, config) {
            console.log(error);
        });
    };    
    
    
  
   
    
    $scope.uploadFile = function(){    	
    	//$("#enviarForm").appendTo("#myForm");
    	//var data = $("#myForm").serialize();
    	var data = new FormData();
    	
    	var fileName = $("#enviarForm").get(0).files[0].name;
    	data.append('file', $("#enviarForm").get(0).files[0], fileName);
    	$.ajax({
    		type: 'POST',
    		url: 'restapi/file/upload',
    		data: data,
    		processData: false,
    		contentType: false,    		
    		success: function (data) {
				//alert("enviado");
				$scope.retornoNombre = data.fileName;
				$scope.retornoNombreOriginal = data.fileNameOriginal;
				//alert(data.fileNameOriginal);
			},
    		error: function(data){
    			alert('Failed');    			
    		},
    		dataType: 'json',
    	});
    	//$("#myForm").submit();      	
    }
    
    $scope.getIdCat = function (value, name) {
        $scope.idCategoria = value;
        $scope.nombreCategoria = name;
        $scope.nombreSubcategoria = "";
        $scope.nombreAccion = ""; 
        $scope.description = "";
    };

    $scope.getIdSubca = function (value, name, description) {
        $scope.idSucategoria = value;
        $scope.nombreSubcategoria = name;
        $scope.description = description;
        $scope.nombreAccion = "";
    };

    $scope.getAction = function (value, name) {
        $scope.accion = value;
        $scope.nombreAccion = name;
    };    
    
    $scope.getDetail = function (value) {
        $scope.textModel = value;
    };
    
   
    $scope.sendTicket = function () { 
    	var ticket = new Object();
        ticket.description = $scope.textModel;
        ticket.caller = $scope.userCaller;
        ticket.language = $scope.languageId;
        ticket.category = $scope.idCategoria;
        ticket.subcategory = $scope.idSucategoria;
        ticket.action = $scope.accion;
        if($scope.retornoNombre != ""){
        	ticket.fileName = $scope.retornoNombre;
        	ticket.fileNameOriginal = $scope.retornoNombreOriginal;
        }  
        
        ticket.reqTyp = $scope.reqTypId;      
        console.log(ticket);
        $http.post('restapi/tickets', ticket).success(function (data, status,headers, config) { 
            if ($scope.idioma == "Español") {
            	alert("The ticket has been sent");
			}else{
				alert("Ticket enviado!");
			}
        	
            document.location.href = '#/form/category';
            location.reload();
        }).error(function (data, status, headers, config) {
            alert("failure message: " + JSON.stringify( {
                data : data
            }));
        });
        
    };
    
});

	

