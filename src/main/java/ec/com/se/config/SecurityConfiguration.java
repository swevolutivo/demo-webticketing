package ec.com.se.config;

import javax.inject.Inject;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsByNameServiceWrapper;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.data.repository.query.SecurityEvaluationContextExtension;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import ec.com.se.security.AuthoritiesConstants;
import ec.com.se.security.CustomAccessDeniedHandler;
import ec.com.se.security.SEConfigurerPreAuthenticatedProcessingFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Inject
    private JHipsterProperties jHipsterProperties;

    @Inject
    private UserDetailsService userDetailsService;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Inject
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(preauthAuthProvider());
    }
    
    @Bean
    public UserDetailsByNameServiceWrapper<PreAuthenticatedAuthenticationToken> userDetailsServiceWrapper() {
     UserDetailsByNameServiceWrapper<PreAuthenticatedAuthenticationToken> wrapper = 
                    new UserDetailsByNameServiceWrapper<>();
     wrapper.setUserDetailsService(userDetailsService);
     return wrapper;
    }
    
    @Bean
    public PreAuthenticatedAuthenticationProvider preauthAuthProvider() {
     PreAuthenticatedAuthenticationProvider preauthAuthProvider = 
      new PreAuthenticatedAuthenticationProvider();
     preauthAuthProvider.setPreAuthenticatedUserDetailsService(userDetailsServiceWrapper());
     return preauthAuthProvider;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
            http.addFilter(getSEConfigurerPreAuthenticatedProcessingFilter())
        	    .csrf().disable()
                .exceptionHandling()
                .accessDeniedHandler(new CustomAccessDeniedHandler())
            .and()
                .headers()
                .frameOptions()
                .disable()
            .and()
                .authorizeRequests()
                .antMatchers("/api/register").permitAll()
                .antMatchers("/api/activate").permitAll()
                .antMatchers("/api/authenticate").permitAll()
                .antMatchers("/api/account/reset_password/init").permitAll()
                .antMatchers("/api/account/reset_password/finish").permitAll()
                .antMatchers("/api/profile-info").permitAll()
                .antMatchers("/api/**").authenticated()
                .antMatchers("/management/**").hasAuthority(AuthoritiesConstants.ADMIN)
                .antMatchers("/v2/api-docs/**").permitAll()
                .antMatchers("/configuration/ui").permitAll()
                .antMatchers("/swagger-ui/index.html").hasAuthority(AuthoritiesConstants.ADMIN);
    }
    
    public SEConfigurerPreAuthenticatedProcessingFilter getSEConfigurerPreAuthenticatedProcessingFilter() throws Exception {
    	SEConfigurerPreAuthenticatedProcessingFilter filter = new SEConfigurerPreAuthenticatedProcessingFilter();
    	filter.setAuthenticationManager(authenticationManager());
    	return filter;
    }

    @Bean
    public SecurityEvaluationContextExtension securityEvaluationContextExtension() {
        return new SecurityEvaluationContextExtension();
    }
}
