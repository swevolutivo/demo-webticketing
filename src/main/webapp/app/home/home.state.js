(function() {
    'use strict';

    angular
        .module('demoApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('home', {
            parent: 'app',
            url: '/',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/home/home.html',
                    controller: 'HomeController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                mainTranslatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate,$translatePartialLoader) {
                    $translatePartialLoader.addPart('home');
                    return $translate.refresh();
                }]
            }
        })
        .state('home.category', {
          url: 'category',
          views: {
              'contenido@': {
                  templateUrl: 'app/home/home.category.html',
              }
          }
        })

        .state('home.detail', {
          url: 'detail',
          views: {
              'contenido@': {
                  templateUrl: 'app/home/home.detail.html',
              }
          }
      })

      .state('home.summary', {
          url: 'summary',
          views: {
              'contenido@': {
                  templateUrl: 'app/home/home.summary.html',
              }
          }
      });
    }
})();
